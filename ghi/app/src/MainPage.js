function MainPage() {
  return (
    <div className="px-2 py-2 my-2 text-center bg-light">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership management!
        </p>
        <img className="bg-white rounded shadow d-block mx-auto mb-4" src="./carmain.jpg" alt="" width="600"></img>
      </div>
    </div>
    
  );
}

export default MainPage;
